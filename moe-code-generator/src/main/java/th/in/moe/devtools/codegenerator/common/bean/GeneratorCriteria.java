package th.in.moe.devtools.codegenerator.common.bean;

import java.util.List;

import th.in.moe.devtools.codegenerator.common.constant.GeneratorConstant.ProfileTemplate;
import th.in.moe.devtools.codegenerator.typeconvert.DbTypeConverter;

public interface GeneratorCriteria {

	public String getDbCatalog();

	public void setDbCatalog(String dbCatalog);

	public String getDbSchema();

	public void setDbSchema(String dbSchema);

	public String getDbTableName();

	public void setDbTableName(String dbTableName);

	public String getResultPath();

	public void setResultPath(String resultPath);

	public String getResultEntityPackage();

	public void setResultEntityPackage(String resultEntityPackage);

	public String getResultRepositoryPackage();

	public void setResultRepositoryPackage(String resultRepositoryPackage);

	public List<String> getExcludeColumn();

	public void setExcludeColumn(List<String> excludeColumn);

	public String getDbProductionName();

	public DbTypeConverter getDbTypeConverter();

	public ProfileTemplate getProfile();

	public void setProfile(ProfileTemplate profile);

	public boolean isGenerateGeneratedValueFlag();

	public void setGenerateGeneratedValueFlag(boolean generateGeneratedValueFlag);

	public boolean isGenerateToStringMethodFlag();

	public void setGenerateToStringMethodFlag(boolean generateToStringMethodFlag);
	
}
