package th.in.moe.devtools.codegenerator.common.bean;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import th.in.moe.devtools.codegenerator.common.properties.GeneratorProperties;
import th.in.moe.devtools.codegenerator.common.util.GeneratorUtils;
import th.in.moe.devtools.codegenerator.persistence.repository.GeneratorRepository;
import th.in.moe.devtools.codegenerator.typeconvert.DbTypeConverter;

@Component
public class GeneratorCriteriaBuilder {
	
	private GeneratorProperties generatorProperties;
	private GeneratorRepository generatorRepository;
	private ApplicationContext appContext;
	
	@Autowired
	public GeneratorCriteriaBuilder(GeneratorProperties generatorProperties,
		GeneratorRepository generatorRepository,
		ApplicationContext appContext) {
		this.generatorProperties = generatorProperties;
		this.generatorRepository = generatorRepository;
		this.appContext = appContext;
	}
	
	@SuppressWarnings("unchecked")
	public GeneratorCriteria build() {
		GeneratorCriteriaDetail criteria = GeneratorCriteriaDetail.getInstance();
		// Database Catalog
//		if (StringUtils.isNotEmpty(generatorProperties.getDb().getCatalog())) {
//			criteria.setDbCatalog(generatorProperties.getDb().getCatalog());
//		} else {
//			criteria.setDbCatalog(generatorRepository.getDatabaseCatalog());
//		}
		// Database Schema
//		if (StringUtils.isNotEmpty(generatorProperties.getDb().getSchema())) {
//			criteria.setDbSchema(generatorProperties.getDb().getSchema());
//		} else {
//			criteria.setDbSchema(generatorRepository.getDatabaseSchema());
//		}
		// Database Table Name Pattern
		if (StringUtils.isNotEmpty(generatorProperties.getDb().getTableName())) {
			criteria.setDbTableName(generatorProperties.getDb().getTableName());
		} else {
			criteria.setDbTableName("%");
		}
		// Result Path
		if (StringUtils.isNotEmpty(generatorProperties.getResultPath())) {
			criteria.setResultPath(generatorProperties.getResultPath());
		}
		// Result Package
		if (StringUtils.isNotEmpty(generatorProperties.getResultPath())) {
			criteria.setResultPath(generatorProperties.getResultPath());
		}
		// Exclude Column
		if (StringUtils.isNotEmpty(generatorProperties.getExcludeColumn())) {
			criteria.setExcludeColumn(CollectionUtils.arrayToList(generatorProperties.getExcludeColumn().split(",")));
		}
		
		// Database Production Name
		criteria.setDbProductionName(generatorRepository.getDatabaseProductName());
		
		// Database Type Converter
		criteria.setDbTypeConverter((DbTypeConverter) appContext.getBean(GeneratorUtils.getDbTypeConverterBeanName(criteria.getDbProductionName())));
		
		// GeneratedValue Flag
		criteria.setGenerateGeneratedValueFlag(Boolean.TRUE);
		
		// ToString Method Flag
		criteria.setGenerateToStringMethodFlag(Boolean.FALSE);
		
		return criteria;
	}
	
}
