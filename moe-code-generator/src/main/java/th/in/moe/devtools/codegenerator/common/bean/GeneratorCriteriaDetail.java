package th.in.moe.devtools.codegenerator.common.bean;

import java.util.List;

import th.in.moe.devtools.codegenerator.common.constant.GeneratorConstant.ProfileTemplate;
import th.in.moe.devtools.codegenerator.typeconvert.DbTypeConverter;

public class GeneratorCriteriaDetail implements GeneratorCriteria {

	private String dbCatalog;
	private String dbSchema;
	private String dbTableName;
	private String resultPath;
	private String resultEntityPackage;
	private String resultRepositoryPackage;
	private List<String> excludeColumn;
	private String dbProductionName;
	private DbTypeConverter dbTypeConverter;
	private ProfileTemplate profile;
	private boolean generateGeneratedValueFlag;
	private boolean generateToStringMethodFlag;

	private static GeneratorCriteriaDetail singleton;

	private GeneratorCriteriaDetail() {
	}

	public static GeneratorCriteriaDetail getInstance() {
		if (singleton == null) {
			singleton = new GeneratorCriteriaDetail();
		}
		return singleton;
	}

	public String getDbCatalog() {
		return dbCatalog;
	}

	public void setDbCatalog(String dbCatalog) {
		this.dbCatalog = dbCatalog;
	}

	public String getDbSchema() {
		return dbSchema;
	}

	public void setDbSchema(String dbSchema) {
		this.dbSchema = dbSchema;
	}

	public String getDbTableName() {
		return dbTableName;
	}

	public void setDbTableName(String dbTableName) {
		this.dbTableName = dbTableName;
	}

	public String getResultPath() {
		return resultPath;
	}

	public void setResultPath(String resultPath) {
		this.resultPath = resultPath;
	}

	public String getResultEntityPackage() {
		return resultEntityPackage;
	}

	public void setResultEntityPackage(String resultEntityPackage) {
		this.resultEntityPackage = resultEntityPackage;
	}

	public String getResultRepositoryPackage() {
		return resultRepositoryPackage;
	}

	public void setResultRepositoryPackage(String resultRepositoryPackage) {
		this.resultRepositoryPackage = resultRepositoryPackage;
	}

	public List<String> getExcludeColumn() {
		return excludeColumn;
	}

	public void setExcludeColumn(List<String> excludeColumn) {
		this.excludeColumn = excludeColumn;
	}

	public String getDbProductionName() {
		return dbProductionName;
	}

	public void setDbProductionName(String dbProductionName) {
		this.dbProductionName = dbProductionName;
	}

	public DbTypeConverter getDbTypeConverter() {
		return dbTypeConverter;
	}

	public void setDbTypeConverter(DbTypeConverter dbTypeConverter) {
		this.dbTypeConverter = dbTypeConverter;
	}

	public ProfileTemplate getProfile() {
		return profile;
	}

	public void setProfile(ProfileTemplate profile) {
		this.profile = profile;
	}

	public boolean isGenerateGeneratedValueFlag() {
		return generateGeneratedValueFlag;
	}

	public void setGenerateGeneratedValueFlag(boolean generateGeneratedValueFlag) {
		this.generateGeneratedValueFlag = generateGeneratedValueFlag;
	}

	public boolean isGenerateToStringMethodFlag() {
		return generateToStringMethodFlag;
	}

	public void setGenerateToStringMethodFlag(boolean generateToStringMethodFlag) {
		this.generateToStringMethodFlag = generateToStringMethodFlag;
	}

}
