package th.in.moe.devtools.codegenerator.common.constant;

/*
 * @Author: Taechapon Himarat (Su)
 * @Create: May 2, 2013
 */
public class GeneratorConstant {
	
	public static final class BEAN {
		public static final class TYPE_CONVERTER {
			public static final String MYSQL = "mysqlTypeConverter";
			public static final String ORACLE = "oracleTypeConverter";
			public static final String DB2 = "db2TypeConverter";
		}
		public static final class PROFILE_TEMPLATE {
			public static final String POJO_PROFILE = "pojoProfile";
			public static final String JPA_ENTITY_PROFILE = "jpaEntityProfile";
			public static final String SPRING_DATA_JPA_PROFILE = "springDataJpaProfile";
			public static final String BUCKWA_JPA_ENTITY_PROFILE = "buckwaJpaEntityProfile";
			public static final String BUCKWA_SPRING_DATA_JPA_PROFILE = "buckwaSpringDataJpaProfile";
		}
	}
	
	public static final class DATABASE_PRODUCTION_NAME {
		public static final String MYSQL = "MySQL";
		public static final String ORACLE = "Oracle";
	}
	
	public enum ProfileTemplate {
		POJO {
			@Override
			public String getBeanName() {
				return BEAN.PROFILE_TEMPLATE.POJO_PROFILE;
			}
		},
		JPA_ENTITY {
			@Override
			public String getBeanName() {
				return BEAN.PROFILE_TEMPLATE.JPA_ENTITY_PROFILE;
			}
		},
		SPRING_DATA_JPA {
			@Override
			public String getBeanName() {
				return BEAN.PROFILE_TEMPLATE.SPRING_DATA_JPA_PROFILE;
			}
		},
		BUCKWA_JPA_ENTITY {
			@Override
			public String getBeanName() {
				return BEAN.PROFILE_TEMPLATE.BUCKWA_JPA_ENTITY_PROFILE;
			}
		},
		BUCKWA_SPRING_DATA_JPA {
			@Override
			public String getBeanName() {
				return BEAN.PROFILE_TEMPLATE.BUCKWA_SPRING_DATA_JPA_PROFILE;
			}
		};
		public abstract String getBeanName();
	}
	
}
