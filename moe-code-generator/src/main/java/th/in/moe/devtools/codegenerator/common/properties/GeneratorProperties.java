package th.in.moe.devtools.codegenerator.common.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("app.generate")
public class GeneratorProperties {

	private Db db;
	private String resultPath;
	private String resultPackage;
	private String excludeColumn;

	public static class Db {
		private String catalog;
		private String schema;
		private String tableName;

		public String getCatalog() {
			return catalog;
		}

		public void setCatalog(String catalog) {
			this.catalog = catalog;
		}

		public String getSchema() {
			return schema;
		}

		public void setSchema(String schema) {
			this.schema = schema;
		}

		public String getTableName() {
			return tableName;
		}

		public void setTableName(String tableName) {
			this.tableName = tableName;
		}
	}

	public Db getDb() {
		return db;
	}

	public void setDb(Db db) {
		this.db = db;
	}

	public String getResultPath() {
		return resultPath;
	}

	public void setResultPath(String resultPath) {
		this.resultPath = resultPath;
	}

	public String getResultPackage() {
		return resultPackage;
	}

	public void setResultPackage(String resultPackage) {
		this.resultPackage = resultPackage;
	}

	public String getExcludeColumn() {
		return excludeColumn;
	}

	public void setExcludeColumn(String excludeColumn) {
		this.excludeColumn = excludeColumn;
	}

}
