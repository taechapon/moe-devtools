package th.in.moe.devtools.codegenerator.common.util;

import th.in.moe.devtools.codegenerator.common.constant.GeneratorConstant.BEAN;
import th.in.moe.devtools.codegenerator.common.constant.GeneratorConstant.DATABASE_PRODUCTION_NAME;

public class GeneratorUtils {
	
	public static String getDbTypeConverterBeanName(String dbVendorName) {
		String beanName = null;
		
		if (DATABASE_PRODUCTION_NAME.MYSQL.equals(dbVendorName)) {
			beanName = BEAN.TYPE_CONVERTER.MYSQL;
		} else if (DATABASE_PRODUCTION_NAME.ORACLE.equals(dbVendorName)) {
			beanName = BEAN.TYPE_CONVERTER.ORACLE;
		}
		
		return beanName;
	}
	
}
