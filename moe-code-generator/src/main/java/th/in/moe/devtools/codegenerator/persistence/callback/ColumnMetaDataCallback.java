package th.in.moe.devtools.codegenerator.persistence.callback;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.support.DatabaseMetaDataCallback;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.jdbc.support.MetaDataAccessException;
import org.springframework.stereotype.Component;

import th.in.moe.devtools.codegenerator.common.bean.ColumnBean;
import th.in.moe.devtools.codegenerator.common.bean.TableBean;
import th.in.moe.devtools.codegenerator.common.util.NameUtils;
import th.in.moe.devtools.codegenerator.typeconvert.DbTypeConverter;

/*
 * @Author: Taechapon Himarat (Su)
 * @Create: Sep 14, 2012
 */
@Component
public class ColumnMetaDataCallback implements DatabaseMetaDataCallback {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private TableBean table;
	
	private DbTypeConverter dbTypeConverter;
	
	public void setProperties(String catalog, String schema, String tableName, DbTypeConverter dbTypeConverter) {
		table = new TableBean();
		table.setTableCatalog(catalog);
		table.setTableSchema(schema);
		table.setTableName(tableName);
		table.setJavaName(NameUtils.toUpperCaseFirstChar(JdbcUtils.convertUnderscoreNameToPropertyName(table.getTableName())));
		this.dbTypeConverter = dbTypeConverter;
	}
	
	@Override
	public Object processMetaData(DatabaseMetaData dbmd) throws SQLException, MetaDataAccessException {
		logger.debug("tableName: {}", table.getTableName());
		
		// Initial
		ColumnBean column = null;
		
		// Key
		ResultSet rs = dbmd.getPrimaryKeys(table.getTableCatalog(), table.getTableSchema(), table.getTableName());
		List<ColumnBean> keyList = new ArrayList<ColumnBean>();
		while (rs.next()) {
			column = new ColumnBean();
			column.setColumnName(rs.getString("COLUMN_NAME"));
			column.setPrimaryKey(Boolean.TRUE);
			keyList.add(column);
			
			logger.debug(
				"key seq : [{}]," +
				"\tcolumn name: [{}]," +
				"\tpk name: [{}]"
			, rs.getShort("KEY_SEQ"), rs.getString("COLUMN_NAME"), rs.getString("PK_NAME"));
		}
		table.setKeyList(keyList);
		
		// Column
		rs = dbmd.getColumns(table.getTableCatalog(), table.getTableSchema(), table.getTableName(), null);
		List<ColumnBean> columnList = new ArrayList<ColumnBean>();
		int index;
		while (rs.next()) {
			column = new ColumnBean();
			column.setColumnName(rs.getString("COLUMN_NAME"));
			
			index = keyList.indexOf(column);
			if (index != -1) {
				column = keyList.get(index);
			}
			
			// Database Column Detail
			column.setDataType(rs.getInt("DATA_TYPE"));
			column.setTypeName(rs.getString("TYPE_NAME"));
			column.setColumnSize(rs.getInt("COLUMN_SIZE"));
			column.setDecimalDigits(rs.getInt("DECIMAL_DIGITS"));
			column.setNumPrecRadix(rs.getInt("NUM_PREC_RADIX"));
			column.setNullable(rs.getInt("NULLABLE"));
			column.setRemarks(rs.getString("REMARKS"));
			column.setColumnDef(rs.getString("COLUMN_DEF"));
			column.setOrdinalPosition(rs.getInt("ORDINAL_POSITION"));
			column.setIsNullable(rs.getString("IS_NULLABLE"));
			//column.setIsAutoIncrement(rs.getString("IS_AUTOINCREMENT"));
			
			// Java Properties Detail
			column.setJavaName(JdbcUtils.convertUnderscoreNameToPropertyName(column.getColumnName()));
			column.setJavaType(dbTypeConverter.convert(column.getDataType(), column.getTypeName()));
			
			if (index == -1) {
				columnList.add(column);
			}
			
			logger.debug(
				"order: [{}]," +
				"\tcolumn name: [{}]," +
				"\ttypeName: [{}]," +
				"\tdataType: [{}]," +
				"\tsize: [{}," +
				"\tdecimalDigits: [{}]"
			, column.getOrdinalPosition(), column.getColumnName(), column.getTypeName()
			, column.getDataType(), column.getColumnSize(), column.getDecimalDigits());
		}
		table.setColumnList(columnList);
		
		return table;
	}

}
