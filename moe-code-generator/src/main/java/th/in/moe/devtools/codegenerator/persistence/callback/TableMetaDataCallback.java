package th.in.moe.devtools.codegenerator.persistence.callback;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.support.DatabaseMetaDataCallback;
import org.springframework.jdbc.support.MetaDataAccessException;
import org.springframework.stereotype.Component;

import th.in.moe.devtools.codegenerator.common.bean.TableBean;

/*
 * @Author: Taechapon Himarat (Su)
 * @Create: Sep 14, 2012
 */
@Component
public class TableMetaDataCallback implements DatabaseMetaDataCallback {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private TableBean table;
	
	public void setProperties(String catalog, String schema, String tableNamePattern) {
		table = new TableBean();
		table.setTableCatalog(catalog);
		table.setTableSchema(schema);
		table.setTableNamePattern(tableNamePattern);
	}
	
	public Object processMetaData(DatabaseMetaData dbmd) throws SQLException, MetaDataAccessException {
		ResultSet rs = dbmd.getTables(table.getTableCatalog(), table.getTableSchema(), table.getTableNamePattern(), new String[] {"TABLE"});
		List<String> tableNameList = new ArrayList<String>();
		while (rs.next()) {
			String name = rs.getString("TABLE_NAME");
			tableNameList.add(name);
			
			if (logger.isDebugEnabled()) {
				logger.debug("Table Name: {}", name);
			}
		}
		
		return tableNameList;
	}

}
