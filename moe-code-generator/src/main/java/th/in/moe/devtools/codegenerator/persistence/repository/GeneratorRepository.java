package th.in.moe.devtools.codegenerator.persistence.repository;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.jdbc.support.MetaDataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import th.in.moe.devtools.codegenerator.common.bean.GeneratorCriteria;
import th.in.moe.devtools.codegenerator.common.bean.TableBean;
import th.in.moe.devtools.codegenerator.common.exception.GeneratedException;
import th.in.moe.devtools.codegenerator.persistence.callback.ColumnMetaDataCallback;
import th.in.moe.devtools.codegenerator.persistence.callback.TableMetaDataCallback;

/*
 * @Author: Taechapon Himarat (Su)
 * @Create: Sep 13, 2012
 */
@Repository
public class GeneratorRepository {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private DataSource dataSource;
	private TableMetaDataCallback tableMetaDataCallback;
	private ColumnMetaDataCallback columnMetaDataCallback;
	
	@Autowired
	public GeneratorRepository(DataSource dataSource,
		TableMetaDataCallback tableMetaDataCallback,
		ColumnMetaDataCallback columnMetaDataCallback) {
		this.dataSource = dataSource;
		this.tableMetaDataCallback = tableMetaDataCallback;
		this.columnMetaDataCallback = columnMetaDataCallback;
	}
	
	public String getDatabaseProductName() {
		String databaseProductName = null;
		try {
			databaseProductName = dataSource.getConnection().getMetaData().getDatabaseProductName();
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
		return databaseProductName;
	}
	
	public String getDatabaseCatalog() {
		String databaseCatalog = null;
		try {
			databaseCatalog = dataSource.getConnection().getCatalog();
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
		return databaseCatalog;
	}
	
	public String getDatabaseSchema() {
		String databaseSchema = null;
		try {
			databaseSchema = dataSource.getConnection().getSchema();
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
		return databaseSchema;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getAllTableName(GeneratorCriteria criteria) throws GeneratedException {
		
		List<String> tableNameList = Collections.emptyList();
		try {
			tableMetaDataCallback.setProperties(criteria.getDbCatalog(), criteria.getDbSchema(), criteria.getDbTableName());
			tableNameList = (List<String>) JdbcUtils.extractDatabaseMetaData(dataSource, tableMetaDataCallback);
			logger.debug("tableNameList: " + StringUtils.collectionToCommaDelimitedString(tableNameList));
		} catch (MetaDataAccessException e) {
			logger.error(e.getMessage(), e);
			throw new GeneratedException(e.getMessage(), e);
		}
		
		return tableNameList;
	}
	
	public TableBean getTableDescribe(GeneratorCriteria criteria, String tableName) throws GeneratedException {
		TableBean table = null;
		columnMetaDataCallback.setProperties(criteria.getDbCatalog(), criteria.getDbSchema(), tableName, criteria.getDbTypeConverter());
		
		try {
			table = (TableBean) JdbcUtils.extractDatabaseMetaData(dataSource, columnMetaDataCallback);
		} catch (MetaDataAccessException e) {
			logger.error(e.getMessage(), e);
			throw new GeneratedException(e.getMessage(), e);
		}
		
		return table;
	}
	
}