package th.in.moe.devtools.codegenerator.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import th.in.moe.devtools.codegenerator.common.bean.GeneratorCriteria;
import th.in.moe.devtools.codegenerator.common.bean.TableBean;
import th.in.moe.devtools.codegenerator.common.exception.GeneratedException;
import th.in.moe.devtools.codegenerator.persistence.repository.GeneratorRepository;
import th.in.moe.devtools.codegenerator.template.profile.ProfileTemplate;

/*
 * @Author: Taechapon Himarat (Su)
 * @Create: May 1, 2013
 */
@Service("generatorService")
public class GeneratorService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private GeneratorRepository generatorRepository;
	private ApplicationContext appContext;
	
	@Autowired
	public GeneratorService(GeneratorRepository generatorRepository,
		ApplicationContext appContext) {
		this.generatorRepository = generatorRepository;
		this.appContext = appContext;
	}

	public List<String> getAllTableName(GeneratorCriteria criteria) throws GeneratedException {
		logger.debug("Inside - getAllTableName()");
		
		List<String> tableNameList = generatorRepository.getAllTableName(criteria);
		
		return tableNameList;
	}

	public List<TableBean> getTableDescribe(GeneratorCriteria criteria, List<String> tableNameList) throws GeneratedException {
		logger.debug("Inside - getTableDescribe()");
		
		List<TableBean> tableList = new ArrayList<TableBean>();
		for (String tableName : tableNameList) {
			TableBean table = generatorRepository.getTableDescribe(criteria, StringUtils.trim(tableName));
			tableList.add(table);
		}
		
		return tableList;
	}
	
	public void genJavaFromTable(List<TableBean> tableList, GeneratorCriteria criteria) throws GeneratedException {
		logger.debug("Inside - genJavaFromTable()");
		
		ProfileTemplate profileTemplate = (ProfileTemplate) appContext.getBean(criteria.getProfile().getBeanName());
		
		for (TableBean table : tableList) {
			profileTemplate.generate(criteria, table);
		}
	}

}
