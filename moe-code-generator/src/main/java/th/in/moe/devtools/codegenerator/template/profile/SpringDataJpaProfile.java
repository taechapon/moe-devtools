package th.in.moe.devtools.codegenerator.template.profile;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.sun.codemodel.JCodeModel;

import th.in.moe.devtools.codegenerator.common.bean.GeneratorCriteria;
import th.in.moe.devtools.codegenerator.common.bean.TableBean;
import th.in.moe.devtools.codegenerator.common.constant.GeneratorConstant.BEAN;
import th.in.moe.devtools.codegenerator.common.exception.GeneratedException;
import th.in.moe.devtools.codegenerator.common.util.FileUtils;
import th.in.moe.devtools.codegenerator.template.JpaEntityTemplate;
import th.in.moe.devtools.codegenerator.template.SpringDataJpaRepositoryTemplate;

/*
 * @Author: Taechapon Himarat (Su)
 * @Create: Jul 25, 2018
 */
@Component(BEAN.PROFILE_TEMPLATE.SPRING_DATA_JPA_PROFILE)
public class SpringDataJpaProfile implements ProfileTemplate {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private JpaEntityTemplate jpaEntityTemplate;
	private SpringDataJpaRepositoryTemplate springDataJpaRepositoryTemplate;
	
	@Autowired
	public SpringDataJpaProfile(JpaEntityTemplate jpaEntityTemplate,
		SpringDataJpaRepositoryTemplate springDataJpaRepositoryTemplate) {
		this.jpaEntityTemplate = jpaEntityTemplate;
		this.springDataJpaRepositoryTemplate = springDataJpaRepositoryTemplate;
	}
	
	@Override
	public void generate(GeneratorCriteria criteria, TableBean table) throws GeneratedException {
		try {
			JCodeModel entityModel = jpaEntityTemplate.execute(criteria, table);
			FileUtils.buildJCodeModel(new File(criteria.getResultPath()), entityModel);
			if (!CollectionUtils.isEmpty(table.getKeyList())) {
				JCodeModel crudRepositoryModel = springDataJpaRepositoryTemplate.execute(criteria, table);
				FileUtils.buildJCodeModel(new File(criteria.getResultPath()), crudRepositoryModel);
			} else {
				logger.warn("Table:{} hasn't primary key. It can't generate Repository class", table.getTableName());
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new GeneratedException(e.getMessage(), e);
		}
	}

}
