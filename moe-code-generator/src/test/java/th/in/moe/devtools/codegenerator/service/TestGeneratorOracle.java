//package th.in.moe.devtools.codegenerator.service;
//
//import java.util.Arrays;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.annotation.AnnotationConfigApplicationContext;
//import org.springframework.util.CollectionUtils;
//
//import th.in.moe.devtools.codegenerator.common.bean.TableBean;
//import th.in.moe.devtools.codegenerator.common.config.AppConfig;
//import th.in.moe.devtools.codegenerator.common.constant.GeneratorConstant.BEAN;
//import th.in.moe.devtools.codegenerator.common.constant.GeneratorConstant.KEY;
//import th.in.moe.devtools.codegenerator.common.exception.GeneratedException;
//import th.in.moe.devtools.codegenerator.typeconvert.DbTypeConverter;
//
//public class TestGeneratorOracle {
//	
//	private static ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
//	
//	private static Map<String, Object> configMap;
//	
//	@BeforeClass
//	public static void beforeClass() {
//		configMap = new HashMap<String, Object>();
//		
//		// DB Parameter
//		configMap.put(KEY.CATALOG, "");
//		configMap.put(KEY.SCHEMA, "SCOTT");
//		configMap.put(KEY.NAME_PATTERN, "MOE_%");
//	}
//	
//	@Before
//	public void before() {
//		System.out.println("##################################################");
//	}
//	
//	@Test
//	public void testListTableName() throws GeneratedException {
//		GeneratorService generatorService = (GeneratorService) context.getBean("generatorService");
//
//		List<String> tableNameList = generatorService.getAllTableName(configMap);
//		
//		for (String tableName : tableNameList) {
//			System.out.println(tableName);
//		}
//	}
//	
//	@Test
//	public void test() throws GeneratedException {
//		DbTypeConverter dbTypeConverter = (DbTypeConverter) context.getBean(BEAN.TYPE_CONVERTER.ORACLE);
//		GeneratorService generatorService = (GeneratorService) context.getBean("generatorService");
//		generatorService.setDbTypeConverter(dbTypeConverter);
//		
//		String[] tableNames = new String[] {
//			"MOE_GENERATOR_TEST"
//		};
//		
//		Map<String, Object> configMap = new HashMap<String, Object>();
//		configMap.put(KEY.PATH, "./src/test/java");
//		configMap.put(KEY.PACKAGENAME, "th.co.baiwa.framework.component.admin.model");
//		configMap.put(KEY.EXCLUDE_COLUMN, CollectionUtils.arrayToList("".split(",")));
//		configMap.put(KEY.CATALOG, "");
//		configMap.put(KEY.SCHEMA, "");
//		
//		// --------------------------------------------------
//		
//		List<String> tableNameList = Arrays.asList(tableNames);
//		//List<String> tableNameList = generatorService.getAllTableName();
//		
//		// Get Table Describe
//		List<TableBean> tableList = generatorService.getTableDescribe(tableNameList, configMap);
//		
//		for (TableBean table : tableList) {
//			System.out.println(table);
//		}
//		
//		// Generate Entity
//		generatorService.genJavaFromTable(tableList, configMap);
//	}
//	
//}
