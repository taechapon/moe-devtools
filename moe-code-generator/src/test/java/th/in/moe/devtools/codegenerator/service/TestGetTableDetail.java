package th.in.moe.devtools.codegenerator.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import th.in.moe.devtools.codegenerator.common.bean.ColumnBean;
import th.in.moe.devtools.codegenerator.common.bean.GeneratorCriteria;
import th.in.moe.devtools.codegenerator.common.bean.GeneratorCriteriaBuilder;
import th.in.moe.devtools.codegenerator.common.bean.TableBean;
import th.in.moe.devtools.codegenerator.common.config.AppConfigTest;
import th.in.moe.devtools.codegenerator.common.constant.GeneratorConstant.ProfileTemplate;
import th.in.moe.devtools.codegenerator.common.exception.GeneratedException;

/*
 * @Author: Taechapon Himarat (Su)
 * @Create: Jun 3, 2015
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppConfigTest.class)
public class TestGetTableDetail {
	
	@Autowired
	private GeneratorCriteriaBuilder generatorCriteriaBuilder;
	
	@Autowired
	private GeneratorService generatorService;
	
	@Test
	public void test() throws GeneratedException {
		
		GeneratorCriteria criteria = generatorCriteriaBuilder.build();
		criteria.setResultPath("./src/test/java");
		criteria.setResultEntityPackage("com.ss.erp.ap.persistence.entity");
		criteria.setResultRepositoryPackage("com.ss.erp.ap.persistence.repository");
		criteria.setProfile(ProfileTemplate.BUCKWA_SPRING_DATA_JPA);
		
		criteria.setDbSchema("TELEMATICS_PP");
		String[] tableNames = new String[]{
			"DATA_ERR_MSG",
			"TB_AS_INBOX"
		};
		criteria.setDbTableName(StringUtils.collectionToCommaDelimitedString(Arrays.asList(tableNames)));
		
		// --------------------------------------------------
		
		List<String> tableNameList = generatorService.getAllTableName(criteria);
		
		// Get Table Describe
		List<TableBean> tableList = generatorService.getTableDescribe(criteria, tableNameList);
		
//		for (TableBean table : tableList) {
//			System.out.println(table);
//		}
		
		// Generate Entity
		generatorService.genJavaFromTable(tableList, criteria);
	}
	
//	@Test
	public void testGenerateJava() throws GeneratedException {
		
		GeneratorCriteria criteria = generatorCriteriaBuilder.build();
		criteria.setResultPath("./src/test/java");
		criteria.setResultEntityPackage("com.ss.erp.ap.persistenct.entity");
		criteria.setExcludeColumn(CollectionUtils.arrayToList("".split(",")));
		criteria.setProfile(ProfileTemplate.POJO);
		
		// Key
		ColumnBean key = new ColumnBean();
		key.setJavaName("objId");
		key.setJavaType(Long.class);
		key.setColumnName("OBJ_ID");
		List<ColumnBean> keyList = new ArrayList<ColumnBean>();
		keyList.add(key);
		
		// Column
		ColumnBean col1 = new ColumnBean();
		col1.setJavaName("sysId");
		col1.setJavaType(Long.class);
		col1.setColumnName("SYS_ID");
		ColumnBean col2 = new ColumnBean();
		col2.setJavaName("parentId");
		col2.setJavaType(Long.class);
		col2.setColumnName("PARENT_ID");
		ColumnBean col3 = new ColumnBean();
		col3.setJavaName("objCode");
		col3.setColumnName("OBJ_CODE");
		col3.setJavaType(String.class);
		ColumnBean col4 = new ColumnBean();
		col4.setJavaName("objName");
		col4.setJavaType(String.class);
		col4.setColumnName("OBJ_NAME");
		ColumnBean col5 = new ColumnBean();
		col5.setJavaName("createdBy");
		col5.setJavaType(String.class);
		col5.setColumnName("CREATED_BY");
		ColumnBean col6 = new ColumnBean();
		col6.setJavaName("createdDt");
		col6.setJavaType(Date.class);
		col6.setColumnName("CREATED_DT");
		ColumnBean col7 = new ColumnBean();
		col7.setJavaName("updatedBy");
		col7.setJavaType(String.class);
		col7.setColumnName("UPDATED_BY");
		ColumnBean col8 = new ColumnBean();
		col8.setJavaName("updatedDt");
		col8.setJavaType(Date.class);
		col8.setColumnName("UPDATED_DT");
		List<ColumnBean> columnList = new ArrayList<ColumnBean>();
		columnList.add(col1);
		columnList.add(col2);
		columnList.add(col3);
		columnList.add(col4);
		columnList.add(col5);
		columnList.add(col6);
		columnList.add(col7);
		columnList.add(col8);
		
		// Table
		TableBean table = new TableBean();
		table.setJavaName("AdmObject");
		table.setTableName("adm_object");
		table.setKeyList(keyList);
		table.setColumnList(columnList);
		List<TableBean> tableList = new ArrayList<TableBean>();
		tableList.add(table);
		
		
		generatorService.genJavaFromTable(tableList, criteria);
	}

}
