package th.in.moe.devtools.codegenerator.service;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import th.in.moe.devtools.codegenerator.common.bean.GeneratorCriteria;
import th.in.moe.devtools.codegenerator.common.bean.GeneratorCriteriaBuilder;
import th.in.moe.devtools.codegenerator.common.config.AppConfigTest;
import th.in.moe.devtools.codegenerator.common.exception.GeneratedException;

/*
 * @Author: Taechapon Himarat (Su)
 * @Create: Jun 3, 2015
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppConfigTest.class)
public class TestGetTableNameList {
	
	@Autowired
	private GeneratorCriteriaBuilder generatorCriteriaBuilder;
	
	@Autowired
	private GeneratorService generatorService;
	
	@Test
	public void test() throws GeneratedException {
		GeneratorCriteria criteria = generatorCriteriaBuilder.build();
		criteria.setDbSchema("TELEMATICS_PP");
		criteria.setDbTableName("DATA_ERR_MSG");
		
		List<String> tableNameList = generatorService.getAllTableName(criteria);
		
		for (String tableName : tableNameList) {
			System.out.println(tableName);
		}
	}

}
